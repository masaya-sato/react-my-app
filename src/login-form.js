import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Button, Paper, TextField, Typography} from '@material-ui/core';

const useStyles = makeStyles({
    container: {
        display: 'flex', height: '100vh', backgroundColor: '#6debef'
    },
    loginBase: {
        width: '300px', height: '200px', margin: 'auto', padding: '10px'
    },
    field: {width: '80%'},
    loginButton: {margin: '15px'}

})

const LoginForm = () => {
    const classes = useStyles();
    return (
        <div className={classes.container}>
            <Paper className={classes.loginBase}>
                <Typography>ログインしてください</Typography>
                <TextField className={classes.field} label='Name' />
                <TextField className={classes.field} label='Password' />
                <Button className={classes.loginButton} variant='contained' color='primary'>
                    ログイン
                </Button>
            </Paper>
        </div>

    )
}
export default LoginForm;