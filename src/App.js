import React from 'react';
import {Toolbar} from '@material-ui/core';
import {BrowserRouter, Route, Link, Switch} from "react-router-dom";

import LoginForm from "./login-form";
import ItemList from './item-list.js';
import Home from './home';
import {makeStyles} from "@material-ui/core/styles";

//original
const useStyles = makeStyles({
    container: {
        backgroundColor: '#eeeeee'
    }
});

const App = () => {
    const classes = useStyles();
    return (
        <BrowserRouter>
            <div className={classes.container}>
                <Toolbar >
                    <Link to='/'>トップ</Link>-
                    <Link to='/login'>ログイン</Link>-
                    <Link to='/list'>一覧</Link>
                </Toolbar>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route path='/login' component={LoginForm}/>
                    <Route path='/list' component={ItemList}/>
                </Switch>
            </div>
        </BrowserRouter>
    );
};

export default App;
