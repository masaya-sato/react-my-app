import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Paper, Button, Typography} from '@material-ui/core';
import {useHistory} from 'react-router-dom'

const useStyles = makeStyles({
    container: {display: 'flex', height: '100vh', background: '#e2dada'},
    content: {margin: '100px 150px'}
});

const Home = () => {
    const classes = useStyles();

    const history = useHistory();
    return (
        <Paper className={classes.container}>
            <div className={classes.content}>
                <Typography variant='button'>やることリストに</Typography>
                <Button variant='outlined' onClick={() => history.push('/login')}>
                    ログイン
                </Button>
            </div>
        </Paper>
    );
};
export default Home;